package com.example.lebadung.network;


import com.example.lebadung.model.Example;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIManager {
    String SERVER_URL= "http://bms.mis.vn/";

    @POST("api/MobileNhanVien/DangNhap")
    @FormUrlEncoded
    Call<Example> dangnhap(@Field("TenDangNhap") String first, @Field("MatKhau") String password, @Field("MaMay") String Mamay, @Field("MaHeDieuHanh") String MaHeDieuHanh, @Field("TokenFirebase") String TokenFirebase);

}
