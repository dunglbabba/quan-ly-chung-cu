
package com.example.lebadung.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Example {

    @SerializedName("ErrorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("ErrorDesc")
    @Expose
    private String errorDesc;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("UserInfo")
    @Expose
    private UserInfo userInfo;
    @SerializedName("DanhSachDonVi")
    @Expose
    private List<DanhSachDonVus> danhSachDonVi = null;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<DanhSachDonVus> getDanhSachDonVi() {
        return danhSachDonVi;
    }

    public void setDanhSachDonVi(List<DanhSachDonVus> danhSachDonVi) {
        this.danhSachDonVi = danhSachDonVi;
    }

}
