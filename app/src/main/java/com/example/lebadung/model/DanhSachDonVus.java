
package com.example.lebadung.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DanhSachDonVus {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Ten")
    @Expose
    private String ten;
    @SerializedName("DiaChi")
    @Expose
    private String diaChi;
    @SerializedName("DuongDanAnh")
    @Expose
    private String duongDanAnh;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getDuongDanAnh() {
        return duongDanAnh;
    }

    public void setDuongDanAnh(String duongDanAnh) {
        this.duongDanAnh = duongDanAnh;
    }

}
