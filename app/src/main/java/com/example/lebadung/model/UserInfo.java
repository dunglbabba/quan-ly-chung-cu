
package com.example.lebadung.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("TenDangNhap")
    @Expose
    private String tenDangNhap;
    @SerializedName("HoVaTen")
    @Expose
    private String hoVaTen;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("SoDienThoai")
    @Expose
    private String soDienThoai;
    @SerializedName("AnhDaiDien")
    @Expose
    private Object anhDaiDien;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenDangNhap() {
        return tenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }

    public String getHoVaTen() {
        return hoVaTen;
    }

    public void setHoVaTen(String hoVaTen) {
        this.hoVaTen = hoVaTen;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public Object getAnhDaiDien() {
        return anhDaiDien;
    }

    public void setAnhDaiDien(Object anhDaiDien) {
        this.anhDaiDien = anhDaiDien;
    }

}
