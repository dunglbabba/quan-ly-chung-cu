package com.example.lebadung.model;

public class Content {
    private String description;
    private String url;

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String geturl(){
        return url;
    }

    public void seturl(String url){
        this.url = url;

    }
}
